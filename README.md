Angular Assessment Ankur_Mishra

This is an Agular project to demonstrate Angular 17 features by displaying image list. To make this project a dessert for eyes and for a good looking UI painting normal CSS is used. It demostrate following operations:

Image lists are displyed in two ways in project one in Grid view and one in List view.

Components conditional rendering is presented by selecting Grid and List view. The respective compenent will render on selected view.

Component communication is presented by sharing state between Parent and Child component.(Grid/List view parent component, Image details displaying Modal child component).

Pagination is demonstrted in two ways in project, Pagination on next and previous button click in Grid view component and Infinite scroll, calling next page api on 60% scroll of page height, scrollbar reposition after getting new page data like facebbok and instagram feeds and infinite scroll. Scroll back to top on single button click.

Creating services and use them in code like API services by HttpClient and fetching data by them in different components.

Getting elements by @ViewChild and Listening events by @HostListener.

Get Started.
To get started needs to get the repo from Github and move into directory to run the project in IDE. Clone the repo.

https://gitlab.com/ankur.mishra.flydocs/angular_assessment_ankur_mishra

Install npm packages install the npm packages and ng serve command will help you to run the project as in localhost.

npm install- npm serve

The npm server commands will build the application and will be run on 4200 port, If the port is already in use it may give you a random port number to demonstrate the application into browser. To close or shut the terminal CTRL + C will be the command.