import { ApplicationConfig, importProvidersFrom } from '@angular/core';
import { provideRouter } from '@angular/router';
import { HttpClientModule } from '@angular/common/http';
import { routes } from './app.routes';
import { ApiServices } from './services/api-services.service';

export const appConfig: ApplicationConfig = {
  providers: [provideRouter(routes), ApiServices, importProvidersFrom(HttpClientModule)]
};