import { Component, EventEmitter, Output, Input} from '@angular/core';
import { CommonModule } from '@angular/common';

@Component({
  selector: 'app-modal',
  standalone: true,
  imports: [CommonModule],
  templateUrl: './popup-modal.component.html',
  styleUrls: ['./popup-modal.component.css']
})
export class ModalComponent {

// GETTING STATES FROM PARENT COMPONENT
  @Input() imgDetails: any;
  @Input() isLoaded : boolean = false

//TO COMMUNICATE WITH PARENT COMPONENT
  @Output() closeModal = new EventEmitter<void>();

  close() {
    this.closeModal.emit();
  }
}
