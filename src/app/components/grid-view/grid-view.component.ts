import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiServices } from '../../services/api-services.service';
import { ModalComponent } from '../popup-modal/popup-modal.component';

@Component({
  selector: 'app-grid-view',
  standalone: true,
  imports: [CommonModule, ModalComponent],
  templateUrl: './grid-view.component.html',
  styleUrl: './grid-view.component.css'
})
export class GridView {

  images: any[] = []
  showPage: number = 1
  isModalOpen: boolean = false;
  imageDetailObj : any
  haveGotDetails : boolean = false

  constructor(private apiService: ApiServices) { }

//FUNCTION ON PAGE LOAD : LOADED FIRST RESPONSE
ngOnInit(){
  this.apiService.getData().subscribe((response) => {
    this.images = response;
  })
}

//FUNCTION TO LOAD DATA AS PROVIDED PAGE NUMBER
  loadPage(page: number){
    this.apiService.getNextPage(page).subscribe((response) => {
      this.images = response;
  })
}

  //FUNCTION TO LOAD SPECIFIC IMAGE DETAIL BY IMAGE ID
  imageDetails(id: number){
    this.apiService.getImageDetails(id).subscribe((response) => {
      this.imageDetailObj = response;
      this.haveGotDetails = true
    })
  }

//FUNCTION TO DO PAGINATION 
  pagination(str:string){
    if(str === 'next'){
      this.showPage++
      this.loadPage(this.showPage)
    } else if(str === 'prev' && this.showPage > 1 ){
      this.showPage--
      this.loadPage(this.showPage)
    }
  }

//FUNCTION TO OPEN OPOUP MODAL
  openModal(id:number) {
    this.isModalOpen = true;
    this.haveGotDetails = false;
    this.imageDetails(id)
  }

//FUNCTION TO CLOSE POPUP MODAL
  closeModal() {
    this.isModalOpen = false;
  }

}
