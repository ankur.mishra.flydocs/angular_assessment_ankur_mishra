import { Component, ElementRef, HostListener, OnInit, ViewChild } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ApiServices } from '../../services/api-services.service';
import { ModalComponent } from '../popup-modal/popup-modal.component';

@Component({
  selector: 'app-list-view',
  standalone: true,
  imports: [CommonModule, ModalComponent],
  templateUrl: './list-view.component.html',
  styleUrl: './list-view.component.css'
})
export class ListView implements OnInit {

// TO GET SCROLL CONTAINER ELEMENT
  @ViewChild('scrollContainer') scrollContainer!: ElementRef;

// TO LISTEN SCROLL EVENT
  @HostListener('window:scroll', [])

//TO PERFORM REQUIRED ACTIONS ON SCROLL
  onScroll(): void {
    this.isScrolled = true
    if (this.shouldFetchData()) {
      this.loadNextPage(this.currentPage);
    }
  }

// REQUIRED VARIABLES DECLARATION
  images: any[] = []
  viewType: string = "List"
  isLoading: Boolean = false
  currentPage = 1;
  isScrolled: Boolean = false
  isModalOpen: boolean = false;
  imageDetailObj: any
  haveGotDetails: boolean = false

  constructor(private apiService: ApiServices) { }

// LOADING PAGE ONE ON INITIAL LOAD
  ngOnInit() {
    this.loadNextPage(this.currentPage);
  }

//CONDITION CHECK TO FETCH DATA ON SCROLL
  shouldFetchData(): boolean {
    const container = this.scrollContainer.nativeElement;
    const scrollTop = container.scrollTop || document.documentElement.scrollTop;
    const scrollHeight = container.scrollHeight || document.documentElement.scrollHeight;
    const clientHeight = container.clientHeight || document.documentElement.clientHeight;
    const scrollY = window.scrollY
    const offsetHeight = document.body.offsetHeight
    return scrollTop + clientHeight >= scrollHeight && Number(scrollY) > Math.round(Number(offsetHeight) * 0.6);
  }

//FUNCTION TO LOAD NEXT PAJE ON SCROLL
  loadNextPage(page: number) {
    if (!this.isLoading) {
      this.isLoading = true;
      this.apiService.getNextPage(page).subscribe((response) => {
        let data = response;
        this.images.push(...data)
        this.isLoading = false
        setTimeout(() => {
          this.currentPage++
          this.isLoading = false
        }, 2000)
      })
    }
  }

//FUNCTION TO FETCH IMAGE DETAILS BY ID
  imageDetails(id: number) {
    this.apiService.getImageDetails(id).subscribe((response) => {
      this.imageDetailObj = response;
      this.haveGotDetails = true
    })
  }

//FUNCTION TO SCROLL TOP OF THE PAGE
  scrollTop () {
    window.scrollTo(0,0)
  }

//FUNCTION TO OPEN IMAGE DETAIL VIEW MODAL
  openModal(id: number) {
    this.isModalOpen = true;
    this.haveGotDetails = false;
    this.imageDetails(id)
  }

//FUNCTION TO CLOSE MODAL
  closeModal() {
    this.isModalOpen = false;
  }

}
