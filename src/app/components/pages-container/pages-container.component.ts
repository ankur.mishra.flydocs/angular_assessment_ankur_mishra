import { Component } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GridView } from '../grid-view/grid-view.component';
import { ListView } from '../list-view/list-view.component';

@Component({
  selector: 'app-pages-container',
  standalone: true,
  imports: [CommonModule, GridView, ListView],
  templateUrl: './pages-container.component.html',
  styleUrl: './pages-container.component.css'
})
export class PagesContainerComponent {

isGridView : Boolean = true;
isListView : Boolean = false;

//FUNCTION TO CONDITIONALLY RENDER GRID VIEW
onGridView () {
  this.isGridView = true;
  this.isListView = false;
}

//FUNCTION TO CONDITIONALLY RENDER LIST VIEW
onListView () {
  this.isGridView = false;
  this.isListView = true;
}

}
