import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApiServices {

  mainURL: string = "https://picsum.photos"
  apiURL: string = "https://picsum.photos/v2/list"

  constructor(private http: HttpClient) { }

//SERVICE TO GET INITIAL DATA FROM API
  getData() {
    return this.http.get<any>(this.apiURL);
  }

//SERVICE TO GET IMAGE DETAIL BY ID
  getImageDetails(id : number) {
    return this.http.get<any>(`${this.mainURL}/id/${id}/info`);
  }

//SERVICE TO GET PAGE NUMBER BY ID
  getNextPage(pageNumber: Number) {
    return this.http.get<any>(`${this.apiURL}?page=${pageNumber}`);
  }

}
